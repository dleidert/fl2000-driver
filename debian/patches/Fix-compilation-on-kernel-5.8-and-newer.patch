From: Marcus Comstedt <marcus@mc.pp.se>
Date: Fri, 21 May 2021 10:36:13 +0200
Subject: Fix compilation on kernel 5.8 and newer

Origin: https://github.com/FrescoLogic/FL2000/pull/67/commits/349da44e45f1bae6ff22928f4ab43c94dd8b7bd1.patch
Bug: https://github.com/FrescoLogic/FL2000/pull/67
Forwarded: not-needed
---
 src/fl2000_surface.c | 30 ++++++++++++++++++++++++------
 1 file changed, 24 insertions(+), 6 deletions(-)

diff --git a/src/fl2000_surface.c b/src/fl2000_surface.c
index 6e10c0d..6175a04 100644
--- a/src/fl2000_surface.c
+++ b/src/fl2000_surface.c
@@ -8,6 +8,20 @@
 #include "fl2000_include.h"
 #include <linux/version.h>
 
+/*
+ * handle mmap_sem change in kernel version 5.8
+ */
+#if LINUX_VERSION_CODE < KERNEL_VERSION(5,8,0)
+
+#define mmap_read_lock(mm) down_read(&mm->mmap_sem)
+#define mmap_read_unlock(mm) up_read(&mm->mmap_sem)
+
+#else
+
+#include <linux/mmap_lock.h>
+
+#endif
+
 /*
  * work-around get_user_pages API changes
  * for kernel version < 4.6.0
@@ -106,13 +120,13 @@ int fl2000_surface_pin_down(
 	case SURFACE_TYPE_VIRTUAL_FRAGMENTED_VOLATILE:
 	case SURFACE_TYPE_VIRTUAL_FRAGMENTED_PERSISTENT:
 		while (surface->pages_pinned != nr_pages) {
-			down_read(&current->mm->mmap_sem);
+			mmap_read_lock(current->mm);
 			pages_pinned = fl2000_get_user_pages(
 				surface->user_buffer,
 				nr_pages,
 				pages,
 				NULL);
-			up_read(&current->mm->mmap_sem);
+			mmap_read_unlock(current->mm);
 			if (pages_pinned <= 0) {
 				dbg_msg(TRACE_LEVEL_ERROR, DBG_PNP,
 					"get_user_pages fails with %d\n", pages_pinned);
@@ -126,7 +140,7 @@ int fl2000_surface_pin_down(
 		break;
 
 	case SURFACE_TYPE_VIRTUAL_CONTIGUOUS:
-		down_read(&current->mm->mmap_sem);
+		mmap_read_lock(current->mm);
 		/*
 		 * work-around the user memory which is mapped from driver,
 		 * but with VM_IO, VM_PFNMAP flags. This API assumes the mmaped user addr
@@ -141,7 +155,7 @@ int fl2000_surface_pin_down(
 			pages,
 			NULL);
 		vma->vm_flags = old_flags;
-		up_read(&current->mm->mmap_sem);
+		mmap_read_unlock(current->mm);
 		if (pages_pinned <= 0) {
 			dbg_msg(TRACE_LEVEL_ERROR, DBG_PNP,
 				"get_user_pages fails with %d\n", pages_pinned);
@@ -228,8 +242,12 @@ int fl2000_surface_map(
 		surface->mapped_buffer = vm_map_ram(
 			surface->pages,
 			surface->nr_pages,
-			-1,
-			PAGE_KERNEL);
+			-1
+#if LINUX_VERSION_CODE < KERNEL_VERSION(5,8,0)
+			/* This argument was removed in 5.8 */
+			, PAGE_KERNEL
+#endif
+			);
 		if (surface->mapped_buffer == NULL) {
 			dbg_msg(TRACE_LEVEL_ERROR, DBG_PNP, "vm_map_ram failed?");
 			ret_val = -ENOMEM;
